declare module '@capacitor/core' {
  interface PluginRegistry {
    DgmongFileDownload: DgmongFileDownloadPlugin;
  }
}

export interface DgmongFileDownloadPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
    test(options: { value: string }): Promise<{ value: string }>;
}
