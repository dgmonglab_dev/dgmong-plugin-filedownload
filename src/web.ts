import { WebPlugin } from '@capacitor/core';
import { DgmongFileDownloadPlugin } from './definitions';

export class DgmongFileDownloadWeb extends WebPlugin implements DgmongFileDownloadPlugin {
  constructor() {
    super({
      name: 'DgmongFileDownload',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
  async test(options: {value: string}): Promise<{value: string}> {
      console.log('Test', options);
      return options;
  }
}

const DgmongFileDownload = new DgmongFileDownloadWeb();

export { DgmongFileDownload };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(DgmongFileDownload);
